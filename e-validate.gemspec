$:.push File.expand_path('../lib', __FILE__)

require 'e/validate/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'e-validate'
  s.version     = E::Validate::VERSION
  s.authors     = ['Denis Papushaev']
  s.email       = ['denqxotl@gmail.com']
  s.homepage    = 'http://github.com/denqxotl/e-validate'
  s.summary     = 'Email validator for Rails'
  s.description = 'E-Mail validator by RFC5322'
  s.license     = 'MIT'
  s.required_ruby_version = '~> 2.3'
  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  s.add_dependency 'rails', '~> 5.0.1'

  s.add_development_dependency 'sqlite3'
end
